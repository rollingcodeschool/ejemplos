import React,{Component} from 'react';
import ListaDeConceptos from './components/ListaDeConceptos'
import './App.css';

class App extends Component{
  constructor(props){
    super(props)
    this.state = {
      conceptosPorVer:['Virtual Dom','Componentes','Propiedades','Estado','Ciclo de vida','Manejo de Eventos','Renderizado Condicional','Mucho mas']
    }
  }


  render(){
    return (
      <div className="App">
        <h1>ReactJS</h1>
        <div>
  
          <ListaDeConceptos
            Titulo = 'Bienvenido Roller'
            Subtitulo = 'Estos son los conceptos que aprenderemos'
            conceptos = {this.state.conceptosPorVer}
          />
        
        <div>
          <h2>Conceptos Ya vistos</h2>
          <ul>
            <li></li>
            <li></li>
            <li></li>
            <li></li>
            <li></li>
            <li></li>
            <li></li>
          </ul>
        </div>
        </div>
      </div>
    );
  }
}

export default App;
