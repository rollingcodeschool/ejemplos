import React from 'react'

const ListaDeConceptos = (props) =>{

 return(
  <div>
   <h2>{props.Titulo}</h2>
   <h3>{props.Subtitulo}</h3>
    <ul>
    {
     props.conceptos.map(concepto=>{
      return <li>{concepto}</li>
     })
    }
   </ul>   
  </div>
 )
}

export default ListaDeConceptos